module.exports = {
    placeholderName: 'TemplatePlaceholderName',
    titlePlaceholder: 'Template title placeholder',
    templateDir: './template',
};