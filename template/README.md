# React Native - Template title placeholder <!-- omit in toc -->

# Table of contents <!-- omit in toc -->

- [1. Project information](#1-project-information)
- [2. Contributors](#2-contributors)

# 1. Project information

- Tag: **RN**
- Type: **React Native**
- Languages: **Typescript, Javascript, Objective-C, Java**
- React Native version: **0.63**

# 2. Contributors

- [Julien Papini](mailto:julien.papini@gmail.com)
