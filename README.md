# Cloud React Native - Typescript template <!-- omit in toc -->

Cloud Custom typescript template for React Native.

```bash
$ npx react-native init ProjectName \
  --template custom-react-native-template-typescript \
  --npm \
  --directory "rn-project-name" \
  --title "Project name"
```
